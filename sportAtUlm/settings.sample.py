"""
Local settings for sport@ulm project.

"""

import os
from .settings_base import *


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '#(iid=)(g0o(fzy_ty2f^c-zsc*h@y2rgblq!b07(a^_!@4$i!'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

ADMINS = (
    ('Cof geek', 'cof-geek-sysadmin@ens.fr')
)

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(PUBLIC_DIR, "static")

MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(PUBLIC_DIR, "media")

# User for ssh login on clipper
SSH_SYNC_USER = ""
# Server for ssh login
# Login should be performed with an ssh key (no password)
SSH_SYNC_SERVER = ""

REDIS_PWD = 'REDIS_DB_CREDENTIALS'
