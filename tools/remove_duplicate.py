#!/usr/bin/env python
# BY cphyc on Sept. 2016
#
# This script queries the database, and removes all duplicates
# according to *first and last name* only, merging them together.
# If any "Sportif" model is linked against any version of the duplicate,
# the Sportif is relinked to the oldest one.
#
# The merging strategy is to keep all information, the information entered for the original
# profile is overriding the other ones.
#
# Enjoy.

import os
import sys
import datetime

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sportAtUlm.settings")
from profilENS.models import User, Departement
from bds.models import Sportif
import django
django.setup()

allUsers = User.objects.all()
first_names = [u.first_name for u in allUsers]
last_names  = [u.last_name  for u in allUsers]
emails      = [u.email      for u in allUsers]
treated     = [False        for u in allUsers]

DRY = True
# iterate over all user
for i in range(len(allUsers)):
    if treated[i]:
        continue

    dupl = filter(lambda N: ((first_names[i] == N[1].first_name) and
                                (last_names[i] == N[1].last_name)),
                     enumerate(allUsers))

    dupl = list(dupl)

    # in case of a duplicate (of fst name + last name), merge them
    if len(dupl) > 1:
        print('%s %s is duplicate:' % (first_names[i], last_names[i]))

        for idb, db in dupl:
            treated[idb] = True

        nSportifs= []
        hasSportif = False

        # get the linked sportif, if any
        linkedSportif = None
        for idb, db in dupl:
            sportif = Sportif.objects.filter(user=db)
            if len(sportif) > 1 or (len(sportif) == 1 and linkedSportif is not None):
                raise Exception()
            elif len(sportif) == 1:
                linkedSportif = sportif[0]

        email = phone = gender = birthdate = cotisation = occupation = username = dpmt = None
        # gather information, from last entered to newly entered
        for idb, db in dupl[::-1]:
            email      = email      if db.email      is None else db.email
            phone      = phone      if db.phone      is None else db.phone
            gender     = gender     if db.gender     is None else db.gender
            birthdate  = birthdate  if db.birthdate  is None else db.birthdate
            cotisation = cotisation if db.cotisation is None else db.cotisation
            occupation = occupation if db.occupation is None else db.occupation
            username   = username   if db.username   is None else db.username
            dpmt       = dpmt       if db.departement is None else db.departement
            print('\t %s:' % idb, db.username, db.phone, db.gender, db.birthdate, db.cotisation, db.occupation, db.departement)
        print('\t     ', username, phone, gender, birthdate, cotisation, occupation, dpmt)

        # update the information of the first account
        print('\tupdating information')
        orig = dupl[0][1]
        orig.username = username
        orig.phone = phone
        orig.gender = gender
        orig.birthdate = birthdate
        orig.cotisation = cotisation
        orig.occupation = occupation
        orig.departement = dpmt
        if not DRY:
            orig.save()

        # relink the sportif to the good user
        if linkedSportif is not None and linkedSportif.user != orig:
            print('\trelinking %s' % linkedSportif)
            if not DRY:
                linkedSportif.user = dupl[0]

        # delete other users
        for idb, db in dupl[1:]:
            print('\tdeleting %s (%s)' % (db, idb))
            if not DRY:
                db.delete()
