# -*- coding: utf-8 -*-
from django import forms
from django.contrib import admin, messages
from django.core.urlresolvers import reverse
from django.conf.urls import url
from django.shortcuts import render
from django.template import RequestContext
from django.contrib.admin.utils import flatten_fieldsets

from bds.models import Sportif, Sport, Event, UsersInEvent, \
                       EventOption, SportTimeSlot, EventTimeSlot,\
                       EventOptionChoice
from bds.filters import boolean_filter_factory
from bds.forms import SportifAdminForm, SportAdminForm,\
                      SportifInEventAdminForm, \
                      UserInEventGenericForm

from profilENS.models import User, GENDER_CHOICES

from shared.utils import get_model_fields
from shared.export import ExportMixin
from nested_inline.admin import NestedStackedInline,\
    NestedModelAdmin


def boolean(description=""):
    """
    Convert a admin class method to a boolean widget,
    with short_description=description
    """
    def decorator(func):
        def wrapper(self, *args, **kwargs):
            return func(self, *args, **kwargs)
        wrapper.boolean = True
        wrapper.short_description = description
        return wrapper
    return decorator


user_fields = get_model_fields(User)


class SportsInline(admin.TabularInline):
    model = Sportif.sports.through
    extra = 0


class SportifAdmin(ExportMixin):
    list_display = ('user', 'gender', 'have_ffsu', 'is_AS_PSL',
                    'have_certificate',
                    'phone', 'email', 'occupation',
                    'cotisation', 'registration_date')
    list_filter = ('have_certificate',
                   boolean_filter_factory('have_ffsu'),
                   boolean_filter_factory("is_AS_PSL"),
                   'registration_date',
                   'cotisation_period'
                   )

    ordering = ['user__last_name', 'user__first_name']

    inlines = [SportsInline]
    form = SportifAdminForm

    search_fields = ['^user__first_name', '^user__last_name']

    @boolean(description="n° FFSU")
    def have_ffsu(self, obj):
        return obj.FFSU_number is not None and obj.FFSU_number != ""

    def gender(self, obj):
        for (shortGender, gender) in GENDER_CHOICES:
            if shortGender == obj.user.gender:
                return gender
        return 'Inconnu'
    gender.short_description = 'Genre'

    @boolean(description="AS PSL")
    def is_AS_PSL(self, obj):
        return obj.ASPSL_number is not None and obj.ASPSL_number != ""

    def respo(self, obj):
        '''Show the sports this sportif is respo'''
        list = ", ".join([sport.name
                          for sport in obj.sports.all()
                          if obj in sport.respo.all()])
        if len(list) == 0:
            list = ""
        return list
    respo.short_description = "Respo"


class SportTimeSlotsInline(admin.TabularInline):
    model = SportTimeSlot
    extra = 1


class SportAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'cotisation_frequency',
                    'respo_name', "sportifs")
    search_fields = ["^name"]
    form = SportAdminForm
    ordering = ["name"]

    inlines = [SportTimeSlotsInline, ]
    exclude = ['time_slots']

    def respo_name(self, obj):
        respos = obj.respo.all()
        respos_urls = []
        for respo in respos:
            url = reverse("admin:bds_sportif_change", args=(respo.id,))
            respos_urls.append('<a href="' + url + '">' + str(respo) + "</a>")
        return "\n".join(respos_urls)
    respo_name.short_description = "Respo(s)"
    respo_name.allow_tags = True

    def sportifs(self, obj):
        return obj.sportif_set.count()


class EventOptionChoiceInline(NestedStackedInline):
    model = EventOptionChoice
    extra = 0


class EventOptionInline(NestedStackedInline):
    model = EventOption
    extra = 1
    inlines = [EventOptionChoiceInline]


class EventTimeSlotsInline(NestedStackedInline):
    model = EventTimeSlot
    extra = 1
    max_num = 1


class EventAdmin(NestedModelAdmin):
    list_display = ('name', 'participants')
    search_fields = ["^name"]
    inlines = [EventOptionInline, EventTimeSlotsInline]

    def participants(self, obj):
        return obj.users.all().count()


class UsersInEventAdmin(ExportMixin):
    list_display = ('user', 'event', 'options_selected', 'payed', 'user_phone',
                    'user_email', 'user_have_certificate', 'remark', 'number')
    list_filter = ('event__name', 'payed')
    search_fields = ["^event__name",
                     "^user__user__first_name",
                     "^user__user__last_name"]
    ordering = ["event__name", "user__user__last_name",
                "user__user__first_name"]
    list_export = ('user',
                   'payed', 'user_email',
                   'user_phone', 'user_have_certificate',
                   'number', 'remark')
    list_export_labels = dict()

    form = SportifInEventAdminForm
    change_list_template = 'admin/change_list_with_event_buttons.html'

    extra_fields = []

    def get_instance_data(self, headers, instance):
        '''Decorate the class to get the options for an event'''
        if hasattr(instance, 'event'):
            event = getattr(instance, 'event')
            choices = []
            # Append choices related to the event
            choices.extend(EventOptionChoice.objects.filter(
                option__event=event))

            def closure(choice):
                def loc(obj):
                    query = obj.choices.filter(id=choice.id)

                    return len(query) == 1
                return loc
            for choice in choices:
                key = 'get_choice_%s_%s' % (choice.option.id, choice.id)

                setattr(self, key, closure(choice))
        return super(UsersInEventAdmin, self).get_instance_data(
            headers, instance)

    def get_export_field(self, *args, queryset=None, **kwargs):
        fields = super(UsersInEventAdmin, self).get_export_field(*args,
                                                                 **kwargs)

        events = set([uie.event for uie in queryset])

        choices = []
        for event in events:
            for choice in EventOptionChoice.objects.filter(
                        option__event=event):
                key = 'get_choice_%s_%s' % (choice.option.id, choice.id)
                choices.append(key)
                self.list_export_labels[key] = str(choice)

        newFields = list(fields)+choices
        return newFields

    def changelist_view(self, request, extra_context=None):
        '''Add more context for template rendering'''
        events = Event.objects.values()
        if extra_context is None:
            extra_context = {}

        extra_context['events'] = events
        return super(UsersInEventAdmin, self).changelist_view(request,
                                                              extra_context)

    @boolean(description="Certificat")
    def user_have_certificate(self, obj):
        return obj.user.have_certificate

    user_have_certificate.short_description = 'Certificat'

    def user_email(self, obj):
        return obj.user.email
    user_email.short_description = 'Email'

    def user_phone(self, obj):
        return obj.user.phone
    user_phone.short_description = 'Téléphone'

    def options_selected(self, obj):
        options = obj.choices.all()
        options_string = []
        total = 0
        for option in options:
            options_string.append(option.description +
                                  ' (' + str(option.price) + '€)')
            total += option.price

        options_string.append('Total: ' + str(total) + '€')
        return ', '.join(options_string)
    options_selected.short_description = 'Options sélectionnées'

    # Extend for prefilled forms
    def get_urls(self):
        urls = super(UsersInEventAdmin, self).get_urls()
        my_urls = [
            url(r'^([0-9]+)/add/$',
                self.admin_site.admin_view(self.event_registration_form)),
            url(r'^([0-9]+)/([0-9]+)/change/$',
                self.admin_site.admin_view(self.event_registration_form))
        ]
        return my_urls + urls

    def event_registration_form(self, request, event, uie=None):
        if uie is not None:
            uieObj = UsersInEvent.objects.get(id=uie)
            evObj = uieObj.event
            initial = dict(
                event=uieObj.id,
                user=uieObj.user.id,
                remark=uieObj.remark
            )

            # Get the options of the event
            options = EventOption.objects.filter(event=evObj)

            for i, option in enumerate(options):
                choice = uieObj.choices.get(option=option)
                initial['options_%s' % i] = choice

            # Convert to a mashable form for generic form
            form = UserInEventGenericForm(initial, event=event)

        evObj = Event.objects.get(id=event)

        successMessage = None

        # Parse the request
        if request.method == 'POST':
            form = UserInEventGenericForm(request.POST, event=event)
            if form.is_valid():
                # Save the form
                uie = form.save()
                successMessage = 'Participation enregistrée'
                if uie.number:
                    successMessage += ' (n°%s)' % uie.number
                else:
                    successMessage += ' (%s)' % uie.user
                    # Return the form once again
                form = UserInEventGenericForm(event=event,
                                              initial={'event': evObj})
        else:
            form = UserInEventGenericForm(event=event,
                                          initial={'event': evObj})

        request.form = form

        prices = [[e.price
                   for e in EventOptionChoice.objects.filter(option=opt)]
                  for opt in EventOption.objects.filter(event=evObj)]
        title = 'Inscription - %s' % evObj

        data = {
            'success_message': successMessage,
            'title': title,
            'form': form,
            'opts': self.model._meta,
            'change': False,
            'is_popup': False,
            'save_as': False,
            'has_delete_permission': True,
            'has_add_permission': False,
            'has_change_permission': False,
            'prices': prices,
            'event': evObj
        }
        return render(request,
                      "admin/event_registration_form.html",
                      data,
                      context_instance=RequestContext(request))


admin.site.register(Sportif, SportifAdmin)
admin.site.register(Sport, SportAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(UsersInEvent, UsersInEventAdmin)
