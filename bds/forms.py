# -*- coding: utf-8 -*-
from django import forms
from django.contrib import messages

from selectable.forms.widgets import AutoCompleteSelectWidget, \
    AutoCompleteSelectMultipleWidget
from selectable.forms import AutoCompleteSelectField

from bds.models import Sportif, Sport, Event, UsersInEvent, EventOption,\
    EventOptionChoice
from bds.lookups import SportifLookup, EventLookup
from profilENS.lookups import UserLookup


class SportifAdminForm(forms.ModelForm):

    class Meta(object):
        model = Sportif
        widgets = {
            'user': AutoCompleteSelectWidget(lookup_class=UserLookup),
        }
        exclude = tuple()


class SportAdminForm(forms.ModelForm):

    class Meta(object):
        model = Sport
        widgets = {
          'respo': AutoCompleteSelectMultipleWidget(lookup_class=SportifLookup),
        }
        exclude = tuple()


class SportifInEventAdminForm(forms.ModelForm):

    class Meta(object):
        model = UsersInEvent
        widgets = {
          'user': AutoCompleteSelectWidget(lookup_class=SportifLookup),
          'event': AutoCompleteSelectWidget(lookup_class=EventLookup),
        }
        exclude = tuple()

    def __init__(self, *args, **kwargs):
        super(SportifInEventAdminForm, self).__init__(*args, **kwargs)
        self.fields.update({
            'foo': forms.CharField(widget=forms.Textarea()),
            'bar': forms.CharField(widget=forms.Textarea()),
        })


class UserInEventGenericForm(forms.Form):
    '''Dynamically generate a form for given set of questions'''
    user = AutoCompleteSelectField(label='User',
                                   lookup_class=SportifLookup,
                                   required=True)
    event = forms.ModelChoiceField(label='Event',
                                   queryset=Event.objects.all(),
                                   widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        if "event" in kwargs:
            event = kwargs.pop('event')
            options = self.get_options(event)
        else:
            options = []

        super(UserInEventGenericForm, self).__init__(
            *args,
            **kwargs)

        for i, option in enumerate(options):
            choices = EventOptionChoice.objects.filter(option=option['id'])
            self.fields['option_%s' % i] = forms.ModelChoiceField(
                queryset=choices,
                label=option['description'])

        self.fields['payed'] = forms.BooleanField(label='Payé',
                                                  required=False,
                                                  initial=False)
        self.fields['remark'] = forms.CharField(label='Remarque',
                                                max_length=1000,
                                                required=False)
        self.fields['number'] = forms.IntegerField(label='Numéro d\'inscription',
                                                   required=False)

    def get_options(self, event):
        # Populate the options for the event
        options = [
            dict(description=e.description,
                 id=e.id,
                 choices=[(-1, 'Aucun')]+[
                     (c.id, "%s (%s€)" % (c.description, c.price))
                     for c in EventOptionChoice.objects.filter(option=e)
                 ])
            for e in EventOption.objects.filter(event_id=event)]

        return options

    def clean(self):
        cleaned_data = super(UserInEventGenericForm, self).clean()
        user = cleaned_data.get('user')
        event = cleaned_data.get('event')
        number = cleaned_data.get('number')

        if UsersInEvent.objects.filter(user=user, event=event).exists():
            errMessage = '%(user)s est déjà inscrit à l\'évènement %(event)s'
            self.add_error('user', errMessage % cleaned_data)

        if number and \
           UsersInEvent.objects.filter(event=event, number=number).exists():
            errMessage = 'Le numéro %(number)s a déjà été attribué'
            self.add_error('number', errMessage % cleaned_data)

        return cleaned_data

    def save(self):
        data = self.cleaned_data

        # Get event
        event = data['event']

        # Get user
        user = data['user']

        # Get remark
        remark = data['remark']

        # Get options
        choices = []
        for key in sorted(data.keys()):
            if 'option_' in key:
                choices.append(data[key])

        # Create UsersInEvent entry
        uie = UsersInEvent()
        uie.user = user
        uie.event = event
        uie.payed = data['payed']
        uie.number = data['number']
        uie.remark = remark
        uie.save()
        for choice in choices:
            uie.choices.add(choice)

        return uie
